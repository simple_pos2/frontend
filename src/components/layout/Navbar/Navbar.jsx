import React, { useEffect } from "react";
import { NavLink, useLocation } from "react-router-dom";

const Navbar = () => {
  const location = useLocation();

  return (
    <>
      <nav className="p-4 bg-blue-400">
        <div className="container mx-auto flex justify-between items-center">
          <a href="/" className="text-white text-lg font-semibold">
            Rizky Resto
          </a>
        </div>
      </nav>
      <nav className="p-4 border-b-2">
        <div className="container mx-auto flex items-center">
          <NavLink
            to="/list-food"
            className={`mx-3 mb-2  hover:text-blue-500 hover: focus:text-blue-500 focus:border-blue-500 focus:outline-none ${
              location.pathname == "/list-food"
                ? "text-blue-500 border-b-2 border-blue-500"
                : "text-gray-500"
            }`}
          >
            Makanan
          </NavLink>
          <NavLink
            to="/"
            className={`mx-3 mb-2  hover:text-blue-500 hover:border-blue-500 focus:text-blue-500 focus:border-blue-500 focus:outline-none ${
              location.pathname == "/"
                ? "text-blue-500 border-b-2 border-blue-500"
                : "text-gray-500"
            }`}
          >
            Transaksi
          </NavLink>
          <NavLink
            to="/history-transaksi"
            className={`mx-3 mb-2 hover:text-blue-500 hover:border-blue-500 focus:text-blue-500 focus:border-blue-500 focus:outline-none ${
              location.pathname == "/history-transaksi"
                ? "text-blue-500 border-b-2 border-blue-500"
                : "text-gray-500"
            }`}
          >
            Histori Transaksi
          </NavLink>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
