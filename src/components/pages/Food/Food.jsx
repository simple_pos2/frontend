import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { formatIDRCurrency } from "../../../../utils";

const Food = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      await fetchData();
    })();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BACKEND_URL_PORT}/product`
      );

      setData(response.data.result.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container mx-auto p-10">
      <p className="text-gray-400">Tambahkan menu makanan yang ada di resto</p>
      <div className="border-2 mt-5 p-10">
        <Link
          to="/add-food"
          className="bg-blue-500 p-3 text-white hover:text-yellow-200"
        >
          + Tambah Menu
        </Link>

        <div className="container mx-auto mt-10">
          <table className="min-w-full bg-white border border-gray-300">
            <thead>
              <tr className="bg-gray-100">
                <th className="py-2 px-4 border-b border-gray-300">#</th>
                <th className="py-2 px-4 border-b border-gray-300">Nama</th>
                <th className="py-2 px-4 border-b border-gray-300">Foto</th>
                <th className="py-2 px-4 border-b border-gray-300">Harga</th>
              </tr>
            </thead>
            <tbody>
              {(data || []).map((item, index) => {
                return (
                  <tr key={index} className="hover:bg-gray-50">
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {index + 1}
                    </td>

                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      <p>{item.name}</p>
                    </td>
                    <td className="py-2 px-4 border-b border-gray-300 text-center flex justify-center">
                      <img
                        src={`${
                          import.meta.env.VITE_BACKEND_URL_PORT
                        }/storage/products/${item.image}`}
                        width={120}
                        alt=""
                      />
                    </td>
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {formatIDRCurrency(item.price)}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Food;
