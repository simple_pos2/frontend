import React from "react";
import { useLocation } from "react-router-dom";
import { formatIDRCurrency } from "../../../../utils";

const PrintContent = () => {
  const location = useLocation();
  const data = location.state ? location.state.data : null;
  const buyerPrice = location.state ? location.state.buyerPrice : null;

  const totalPrice = data.reduce((acc, item) => acc + item.price * item.qty, 0);

  return (
    <div className="printable p-10">
      <table className="min-w-full bg-white border border-gray-300">
        <thead>
          <tr className="bg-gray-100">
            <th className="py-2 px-4 border-b border-gray-300">#</th>
            <th className="py-2 px-4 border-b border-gray-300">Nama</th>
            <th className="py-2 px-4 border-b border-gray-300">Foto</th>
            <th className="py-2 px-4 border-b border-gray-300">Qty</th>
            <th className="py-2 px-4 border-b border-gray-300">Total Harga</th>
          </tr>
        </thead>
        <tbody>
          {(data || []).map((item, index) => (
            <tr key={index} className="hover:bg-gray-50">
              <td className="py-2 px-4 border-b border-gray-300 text-center">
                {index + 1}
              </td>
              <td className="py-2 px-4 border-b border-gray-300 text-center">
                <p>{item.name}</p>
              </td>
              <td className="py-2 px-4 border-b border-gray-300 text-center flex justify-center align-middle items-center">
                <img
                  src={`${
                    import.meta.env.VITE_BACKEND_URL_PORT
                  }/storage/products/${item.image}`}
                  width="20%"
                  alt=""
                />
              </td>
              <td className="py-2 px-4 border-b border-gray-300 text-center">
                {item.qty}
              </td>
              <td className="py-2 px-4 border-b border-gray-300 text-center">
                {formatIDRCurrency(item.price * item.qty)}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <div className="mt-10 text-center">
        <p className="font-bold">
          Uang Pembeli : {formatIDRCurrency(buyerPrice)}
        </p>

        <p className="font-bold">
          Total yang harus di bayar: {formatIDRCurrency(totalPrice)}
        </p>

        <p className="font-bold">
          Uang Kembalian: {formatIDRCurrency(buyerPrice - totalPrice)}
        </p>
      </div>

      <div className="flex justify-center mt-10">
        <button
          className="bg-blue-500 p-2 rounded-sm text-white hover:text-yellow-300"
          onClick={() => window.print()}
        >
          Print
        </button>
      </div>
    </div>
  );
};

export default PrintContent;
