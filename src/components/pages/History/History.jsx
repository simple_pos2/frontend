import axios from "axios";
import React, { useState, useEffect } from "react";
import { formatIDRCurrency, formatMySQLTimestamp } from "../../../../utils";

const History = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      await fetchData();
    })();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BACKEND_URL_PORT}/order-detail`
      );

      setData(response.data.result.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container mx-auto p-10">
      <p className="text-gray-400">Histori transaksi yaang sudah di lakukan</p>
      <div className="border-2 mt-5 p-10 overflow-x-auto">
        <div className="container mx-auto">
          <table className="min-w-full bg-white border border-gray-300">
            <thead>
              <tr className="bg-gray-100">
                <th className="py-2 px-4 border-b border-gray-300">#</th>
                <th className="py-2 px-4 border-b border-gray-300">Order ID</th>
                <th className="py-2 px-4 border-b border-gray-300">
                  Total yang harus dibayarkan
                </th>
                <th className="py-2 px-4 border-b border-gray-300">
                  Nominal yang dibayarkan
                </th>
                <th className="py-2 px-4 border-b border-gray-300">
                  Uang kembalian
                </th>
                <th className="py-2 px-4 border-b border-gray-300">
                  Dibuat Tanggal
                </th>
              </tr>
            </thead>
            <tbody>
              {(data || []).map((item, index) => {
                return (
                  <tr key={index} className="hover:bg-gray-50">
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {index + 1}
                    </td>

                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      <p>OD-{item.id}</p>
                    </td>
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {formatIDRCurrency(
                        (parseFloat(item.money_payment) || 0) +
                          (parseFloat(item.money_back_payment) || 0)
                      )}
                    </td>
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      <p>{formatIDRCurrency(item.money_payment || 0)}</p>
                    </td>
                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {formatIDRCurrency(item.money_back_payment || 0)}
                    </td>

                    <td className="py-2 px-4 border-b border-gray-300 text-center">
                      {formatMySQLTimestamp(item.created_at)}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default History;
