import React, { useState } from "react";
import { onInputChange } from "../../../../utils";
import { ToastContainer, toast } from "react-toastify";
import axios from "axios";

const AddFood = () => {
  const [menu, setMenu] = useState({
    name: "",
    price: 0,
  });

  const [image, setImage] = useState({
    file: null,
    url: "",
  });

  const handleImageFile = (e) => {
    const file = e.target.files[0];

    if (!file) return;

    setImage({
      file,
      url: URL.createObjectURL(file),
    });
  };

  const insertData = async () => {
    if (!menu.name || !menu.price || !image.file || !image.url) return;

    try {
      const formData = new FormData();
      formData.append("name", menu.name);
      formData.append("price", menu.price);
      formData.append("image", image.file);

      await axios.post(
        `${import.meta.env.VITE_BACKEND_URL_PORT}/product/create`,
        formData
      );

      setMenu({
        name: "",
        price: 0,
      });

      setImage({
        file: null,
        url: "",
      });

      toast.success("Data makanan disimpan", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />

      <div className="container mx-auto p-10">
        <div className="border-2 mt-5 p-10">
          <p className="text-xl text-blue-500">Tambahkan Menu</p>

          <div className="container mx-auto mt-10 mb-10">
            <div className="mb-4">
              <label htmlFor="namaMenu" className="block text-lg ">
                Nama Menu
              </label>
              <input
                type="text"
                id="namaMenu"
                value={menu.name}
                onChange={(e) =>
                  onInputChange(menu, setMenu, "name", e.target.value)
                }
                placeholder="ex: Sate Ayam"
                className="mt-1 p-2 border border-gray-300 rounded-md w-full"
              />
            </div>

            <div className="mb-4">
              <label htmlFor="uploadGambar" className="block text-lg ">
                Upload Gambar
              </label>
              <input
                type="file"
                id="uploadGambar"
                onChange={(e) => handleImageFile(e)}
                className="mt-1 p-2 border border-gray-300 rounded-md w-full"
              />
            </div>

            <div className="mb-4">
              <label htmlFor="harga" className="block text-lg ">
                Harga
              </label>
              <input
                type="number"
                id="harga"
                value={menu.price}
                onChange={(e) =>
                  onInputChange(menu, setMenu, "price", e.target.value)
                }
                className="mt-1 p-2 border border-gray-300 rounded-md w-full"
              />
            </div>

            <div className="flex justify-end">
              <button
                onClick={insertData}
                className="bg-green-600 p-3 w-36 rounded-sm text-white hover:text-yellow-200"
              >
                + Tambahkan
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddFood;
