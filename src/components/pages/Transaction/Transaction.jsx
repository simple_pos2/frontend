import React, { useEffect, useState } from "react";
import { FaUser } from "react-icons/fa";
import { formatIDRCurrency, onInputChange } from "../../../../utils";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Transaction = () => {
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [dataMenu, setDataMenu] = useState([]);
  const [transaction, setTransaction] = useState({
    /* status paid = 1 , status not paid yet = 0 */
    status: 0,
    buyerPrice: 0,
    totalPrice: 0,
  });

  useEffect(() => {
    (async () => {
      await fetchData();
    })();

    const totalPrice = dataMenu.reduce(
      (acc, itm) => acc + itm.price * itm.qty,
      0
    );

    setTransaction({ ...transaction, totalPrice });
  }, [dataMenu]);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BACKEND_URL_PORT}/product`
      );

      setData(response.data.result.data);
    } catch (err) {
      console.log(err);
    }
  };

  const pushDataMenu = (item) => {
    const findExistsData = dataMenu.find((itm) => itm.id == item.id);

    if (findExistsData && Object.values(findExistsData).length > 0) {
      const updatedDataMenu = dataMenu.map((menu) =>
        menu.id === item.id ? { ...menu, qty: menu.qty + 1 } : menu
      );

      return setDataMenu(updatedDataMenu);
    }

    return setDataMenu([
      ...dataMenu,
      {
        ...item,
        qty: 1,
      },
    ]);
  };

  const openModal = () => {
    if (dataMenu.length == 0) return;

    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const clickButtonClearCart = () => {
    setDataMenu([]);
    setTransaction({
      status: 0,
      buyerPrice: 0,
      totalPrice: 0,
    });
  };

  const clickButtonCetak = () => {
    if (transaction.status == 0) return;
    navigate("/print", {
      state: { data: dataMenu, buyerPrice: transaction.buyerPrice },
    });
  };

  const clibkButtonSaveBill = async () => {
    if (transaction.status == 0) return;

    try {
      const data = {
        products: dataMenu,
        money_payment: Number(transaction.buyerPrice),
        money_back_payment: transaction.buyerPrice - transaction.totalPrice,
      };

      await axios.post(
        `${import.meta.env.VITE_BACKEND_URL_PORT}/order-detail/create`,
        data,
        {
          withCredentials: true,
        }
      );

      setDataMenu([]);
      setTransaction({
        status: 0,
        buyerPrice: 0,
        totalPrice: 0,
      });

      toast.success("Transaksi tersimpan", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } catch (err) {
      console.log(err);
    }
  };

  const clickButtonPaid = () => {
    if (
      dataMenu.length == 0 ||
      Math.sign(transaction.buyerPrice - transaction.totalPrice) === -1
    ) {
      return;
    }

    setTransaction({
      ...transaction,
      status: 1,
    });

    setModalOpen(false);

    toast.success("Transaksi telah di bayar", {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  };

  return (
    <>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />

      <div className="container mx-auto p-3">
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
          <div className="p-4">
            <div className="grid grid-cols-3 gap-10">
              {data.map((item, index) => {
                return (
                  <div key={index} onClick={() => pushDataMenu(item)}>
                    <div className="flex justify-center flex-col border-2 hover:cursor-pointer">
                      <img
                        src={`${
                          import.meta.env.VITE_BACKEND_URL_PORT
                        }/storage/products/${item.image}`}
                        width="100%"
                        alt=""
                      />

                      <div className="mt-5 text-center">
                        <p className="font-bold">{item.name}</p>
                        <p className="mt-3 text-blue-500 pb-5">
                          {formatIDRCurrency(item.price)}
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="p-4 mb-5">
            <div className="border-2">
              <div className="text-center text-2xl mb-10 mt-10 flex items-center justify-center">
                <div className="rounded-md border-5 border-yellow flex items-center">
                  <span className="me-3 inline-block rounded-2xl border-2 border-black p-2">
                    <FaUser className="text-xl" />
                  </span>
                  <p className="text-xl">Pesanan</p>
                </div>
              </div>

              <div className="p-5 grid grid-cols-1 gap-4">
                {dataMenu.map((item, index) => (
                  <div key={index} className="flex border-2 justify-between">
                    <div className="flex items-center">
                      <img
                        src={`${
                          import.meta.env.VITE_BACKEND_URL_PORT
                        }/storage/products/${item.image}`}
                        alt=""
                        width={200}
                      />

                      <p className="font-bold ms-5">{item.name}</p>
                    </div>
                    <div className="flex align-middle items-center">
                      <p className="font-bold mx-3">X{item.qty}</p>
                      <p className="text-blue-500 me-10">
                        {formatIDRCurrency(item.price * item.qty)}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>

            <div className="p-5">
              <button
                onClick={clickButtonClearCart}
                className="p-1 bg-red-500 text-white w-full hover:border-2 hover:border-red-300 hover:text-black hover:bg-white rounded-sm"
              >
                Bersihkan Keranjang
              </button>

              <div className="grid grid-cols-2 gap-2 mt-3">
                <button
                  onClick={clibkButtonSaveBill}
                  className="bg-green-600 w-full text-white p-1 rounded-sm hover:text-yellow-200"
                >
                  Simpan
                </button>
                <button
                  onClick={clickButtonCetak}
                  className="bg-green-600 w-full text-white p-1 rounded-sm hover:text-yellow-200"
                >
                  Cetak
                </button>
              </div>

              <div>
                <button
                  onClick={openModal}
                  className="mt-3 bg-blue-400 w-full p-2 text-white hover:text-yellow-200"
                >
                  Charge {formatIDRCurrency(transaction.totalPrice)}
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className={`fixed inset-0 z-50 overflow-auto ${
            isModalOpen ? "block" : "hidden"
          } bg-black bg-opacity-50`}
        >
          <div className="flex items-center justify-center min-h-screen">
            <div className="bg-white w-1/2 p-6 rounded-lg shadow-lg">
              <h3 className="text-xl mb-5">Detail Pesanan</h3>
              <div className="grid lg:grid-cols-2 md:grid-cols-1 lg:gap-48 md:20 overflow-x-auto">
                <div>
                  <table className="min-w-full bg-white border border-gray-300">
                    <thead>
                      <tr className="bg-gray-100">
                        <th className="py-2 px-4 border-b border-gray-300">
                          #
                        </th>
                        <th className="py-2 px-4 border-b border-gray-300">
                          Nama
                        </th>
                        <th className="py-2 px-4 border-b border-gray-300">
                          Foto
                        </th>
                        <th className="py-2 px-4 border-b border-gray-300">
                          Qty
                        </th>
                        <th className="py-2 px-4 border-b border-gray-300">
                          Harga
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {(dataMenu || []).map((item, index) => {
                        return (
                          <tr key={index} className="hover:bg-gray-50">
                            <td className="py-2 px-4 border-b border-gray-300 text-center">
                              {index + 1}
                            </td>

                            <td className="py-2 px-4 border-b border-gray-300 text-center">
                              <p>{item.name}</p>
                            </td>
                            <td className="py-2 px-4 border-b border-gray-300 text-center flex justify-center align-middle items-center">
                              <img
                                src={`${
                                  import.meta.env.VITE_BACKEND_URL_PORT
                                }/storage/products/${item.image}`}
                                width="100%"
                                alt=""
                              />
                            </td>
                            <td className="py-2 px-4 border-b border-gray-300 text-center">
                              {item.qty}
                            </td>
                            <td className="py-2 px-4 border-b border-gray-300 text-center">
                              {formatIDRCurrency(item.price * item.qty)}
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
                <div>
                  <div className="text-center">
                    <label htmlFor="namaMenu" className="text-center">
                      Uang Pembeli{" "}
                      {`(${formatIDRCurrency(transaction.buyerPrice)})`}
                    </label>
                    <input
                      type="number"
                      id="namaMenu"
                      value={transaction.buyerPrice}
                      onChange={(e) =>
                        onInputChange(
                          transaction,
                          setTransaction,
                          "buyerPrice",
                          e.target.value
                        )
                      }
                      className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                  </div>
                  <div className="grid grid-cols-2 gap-2 my-3">
                    <button
                      onClick={closeModal}
                      className="border-2 p-1 bg-red-500 text-white hover:text-yellow-500"
                    >
                      Close
                    </button>

                    <button
                      disabled={
                        transaction.buyerPrice - transaction.totalPrice < 0
                      }
                      onClick={clickButtonPaid}
                      className={`p-1 border-2  ${
                        transaction.buyerPrice - transaction.totalPrice < 0
                          ? "disabled text-black"
                          : "bg-blue-500 text-white hover:text-yellow-300"
                      }`}
                    >
                      Bayar
                    </button>
                  </div>
                  <p className="text-red-500">
                    Kembalian :{" "}
                    {formatIDRCurrency(
                      transaction.buyerPrice - transaction.totalPrice
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Transaction;
