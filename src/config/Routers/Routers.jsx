import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Transaction, History, Food, AddFood, PrintContent } from "../../components/pages";
import { Navbar } from "../../components/layout/index";

const Routers = () => {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Transaction />} />
        <Route path="/list-food" element={<Food />} />
        <Route path="/add-food" element={<AddFood />} />
        <Route path="/history-transaksi" element={<History />} />
        <Route path="/print" element={<PrintContent />} />
      </Routes>
    </Router>
  );
};

export default Routers;
