import Routers from "./config/Routers/Routers";

function App() {
  return (
    <div>
      <Routers />
    </div>
  );
}

export default App;
