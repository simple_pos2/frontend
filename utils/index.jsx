export const onInputChange = (state, setState, key, value) => {
  return setState({
    ...state,
    [key]: value,
  });
};

export const formatMySQLTimestamp = (mysqlTimestamp) => {
  const jsDate = new Date(mysqlTimestamp);

  const daysOfWeek = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
  ];
  const months = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];

  const dayOfWeek = daysOfWeek[jsDate.getDay()];
  const day = jsDate.getDate();
  const month = months[jsDate.getMonth()];
  const year = jsDate.getFullYear();
  const hour = jsDate.getHours();
  const minute = jsDate.getMinutes();

  const period = hour >= 12 ? "PM" : "AM";
  const formattedHour = hour % 12 === 0 ? 12 : hour % 12;

  const formattedString = `${dayOfWeek} ${day} ${month} ${year} ${formattedHour}:${minute} ${period}`;

  return formattedString;
};

export const formatIDRCurrency = (amount) => {
  const formatter = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 2,
  });

  return formatter.format(amount);
};
